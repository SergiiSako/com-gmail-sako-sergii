package ua.ort.javabase;

import ua.ort.javabase.services.StringUtils;
import ua.ort.javabase.services.StringUtilsImpl;
import ua.ort.javabase.ui.ConsoleUI;
import ua.ort.javabase.ui.UI;

public class HW_24 {

   public static void main(String[] args) {
      StringUtils stringUtils = new StringUtilsImpl();
      UI ui = new ConsoleUI(stringUtils);
      ui.start();

   }
}
