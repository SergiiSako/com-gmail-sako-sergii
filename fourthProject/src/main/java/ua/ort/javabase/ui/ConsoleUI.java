package ua.ort.javabase.ui;

import ua.ort.javabase.services.StringUtils;

public class ConsoleUI implements UI {

   private StringUtils stringUtils;

   public ConsoleUI(StringUtils stringUtils) {
      this.stringUtils = stringUtils;
   }

   public void start() {
      String s1 = "Человек устал, но был доволен что он смог сделать все запланированное.";
      String s2 = "Мудрый человек не делает другим того, чего он не желает, чтобы ему сделали.";

      arrayPrint(stringUtils.arrayComparator(stringUtils.split(stringUtils.clean(s1)),
            stringUtils.split(stringUtils.clean(s2))));

   }

   /**
    * вывод двумерного массива
    * 
    * @param s
    */

   public void arrayPrint(String[][] s) {
      for (int i = 0; i < s.length; i++) {
         for (int j = 0; j < s[i].length; j++) {
            System.out.print(s[i][j] + "\t");
         }
         System.out.println();
      }
   }
}
