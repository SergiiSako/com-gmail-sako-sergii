package ua.ort.javabase;

import ua.ort.javabase.services.ArrayListImpl;
import ua.ort.javabase.services.LinkedListImpl;
import ua.ort.javabase.services.Utils;

public class HW_26 {
   public static void main(String... args) {
      Utils arrayList = new ArrayListImpl();
      Utils linkedList = new LinkedListImpl();
      arrayList.addHead();
      arrayList.addEnd();
      arrayList.addBetween();
      System.out.println();
      arrayList.removeHead();
      arrayList.removeEnd();
      arrayList.removeBetween();
      System.out.println();
      linkedList.addHead();
      linkedList.addEnd();
      linkedList.addBetween();
      System.out.println();
      linkedList.removeHead();
      linkedList.removeEnd();
      linkedList.removeBetween();

   }
}
