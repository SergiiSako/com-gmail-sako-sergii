package ua.ort.javabase.services;

import java.util.ArrayList;
import java.util.List;

public class ArrayListImpl extends Utils {
   protected List<Integer> data;
   protected List<Integer> data1;
   protected List<Integer> data2;
   @Override
   public void addHead() {

//    выполнения вставки с начала
      start = System.currentTimeMillis();
      data = new ArrayList<Integer>();
      for (int i = 0; i < ITERATION; i++) {
         data.add(0, i);
      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения вставки с начала ArrayList = " + (end - start));
   }

   @Override
   public void addEnd() {
      // выполнения вставки с конца
      start = System.currentTimeMillis();
      data1 = new ArrayList<Integer>();
      for (int i = 0; i < ITERATION; i++) {
         data1.add(i);
       
      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения вставки c конца ArrayList = " + (end - start));
   }

   @Override
   public void addBetween() {
      // выполнения вставки в середину
      start = System.currentTimeMillis();
      data2 = new ArrayList<Integer>();

      for (int i = 0; i < ITERATION; i++) {
         data2.add((data2.size() / 2), i);
      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения вставки в средину ArrayList = " + (end - start));
   }

   @Override
   public void removeHead() {
//    выполнение удаления с начала
      start = System.currentTimeMillis();
      for (int i = 0; i > ITERATION; i++) {
         data.remove(0);
      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения удаления с начала ArrayList = " + (end - start));
   }

   @Override
   public void removeEnd() {
//    выполнение удаления с конца
      start = System.currentTimeMillis();
      for (int i = 0; i < ITERATION; i++) {
         data1.remove(data1.size()-1);
                 
      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения удаления с конца ArrayList = " + (end - start));

   }

   @Override
   public void removeBetween() {
      // выполнение удаления из середины
      start = System.currentTimeMillis();
      for (int i = 0; i < ITERATION; i++) {
         data2.remove((data2.size() / 2));
      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения удаления ArrayList = " + (end - start));
   }

}
