package ua.ort.javabase.services;

public abstract class Utils {
   
   protected final int ITERATION = 100000;
   protected long start;
   protected long end;
   
   public abstract void addHead();
   public abstract void addEnd();
   public abstract void addBetween();
   
   public abstract void removeHead();
   public abstract void removeEnd();
   public abstract void removeBetween();
   

}
