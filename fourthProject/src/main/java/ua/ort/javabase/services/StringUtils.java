package ua.ort.javabase.services;

public interface StringUtils {
   public String clean(String s);
   public String[] split(String string);
   public String[][] arrayComparator(String[] s1Array, String[] s2Array);
 
}
