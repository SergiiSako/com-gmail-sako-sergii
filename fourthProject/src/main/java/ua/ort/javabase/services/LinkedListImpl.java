package ua.ort.javabase.services;

import java.util.LinkedList;

public class LinkedListImpl extends Utils {
   private LinkedList<Integer> data;
   private LinkedList<Integer> data1;
   private LinkedList<Integer> data2;

   @Override
   public void addHead() {
//    выполнения вставки с начала
      start = System.currentTimeMillis();
      data = new LinkedList<Integer>();
      for (int i = 0; i < ITERATION; i++) {
         data.addFirst(i);

      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения вставки с начала LikedList = " + (end - start));

   }

   @Override
   public void addEnd() {
      // выполнения вставки с конца
      start = System.currentTimeMillis();
      data1 = new LinkedList<Integer>();
      for (int i = 0; i < ITERATION; i++) {
         data1.addLast(i);

      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения вставки c конца LinkedList = " + (end - start));

   }

   @Override
   public void addBetween() {
      // выполнения вставки в середину
      start = System.currentTimeMillis();
      data2 = new LinkedList<Integer>();
      for (int i = 0; i < ITERATION; i++) {
         data2.add((data2.size() / 2), i);

      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения вставки в средину Linked List = " + (end - start));

   }

   @Override
   public void removeHead() {
//    выполнение удаления с начала
      start = System.currentTimeMillis();
      for (int i = 0; i > ITERATION; i++) {
         data.remove(0);
      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения удаления с начала LinkedList = " + (end - start));

   }

   @Override
   public void removeEnd() {
//    выполнение удаления с конца
      start = System.currentTimeMillis();
      for (int i = 0; i < ITERATION; i++) {
         data1.remove(data1.size() - 1);

      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения удаления с конца LinkedList = " + (end - start));

   }

   @Override
   public void removeBetween() {
      // выполнение удаления из середины
      start = System.currentTimeMillis();
      for (int i = 0; i < ITERATION; i++) {
         data2.remove((data2.size() / 2));
      }
      end = System.currentTimeMillis();
      System.out.println("Время выполнения удаления LinkedList = " + (end - start));
   }

}
