package ua.ort.javabase.services;

public class StringUtilsImpl implements StringUtils {

   /**
    * убирает знаки пунктуации
    * 
    * @param s
    * @return String
    */
   public String clean(String s) {
      String temp = new String();
      String buf = new String();
      temp = s.replace(".", "");
      buf = temp.replace(",", "");
      return buf;
   }

   /**
    * выделяет слова в массив
    * 
    * @param string
    * @return String[]
    */
   public String[] split(String string) {
      return (string.split(" "));
   }

   /**
    * сравнение слов из массивов
    * 
    * @param s1Array
    * @param s2Array
    * @return String[][]
    */

   public String[][] arrayComparator(String[] s1Array, String[] s2Array) {
      String[][] arrayComparator = new String[s1Array.length][s2Array.length];
      for (int i = 0; i < s1Array.length; i++) {
         for (int j = 0; j < s2Array.length; j++) {
            arrayComparator[i][j] = String.valueOf(s1Array[i].equals(s2Array[j]));
         }
      }
      return arrayComparator;
   }

}
