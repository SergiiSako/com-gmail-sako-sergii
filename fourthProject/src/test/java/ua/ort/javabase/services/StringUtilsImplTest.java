package ua.ort.javabase.services;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsImplTest {
   @Test
   public void cleanTest() {
      String s = "Город, который только строится.";
      StringUtilsImpl sc = new StringUtilsImpl();
      String result = sc.clean(s);

      Assert.assertEquals("Город который только строится", result);
   }

   @Test
   public void splitTest() {
      String s = "Город который только строится";
      String[] splitS = { "Город", "который", "только", "строится" };
      StringUtilsImpl sc = new StringUtilsImpl();
      String[] result = sc.split(s);
      Assert.assertEquals(splitS, result);
   }
}
