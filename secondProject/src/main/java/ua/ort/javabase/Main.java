package ua.ort.javabase;

import ua.ort.javabase.enteties.Point;

public class Main {

   public static void main(String[] args) {
      Point sun = new Point("Sun", 10, 10);
      Point moon = new Point("Moon", 1, 2);
      Point mars = new Point("Mars", 9, 1);
      Point venera = new Point("Venera", 3, 2);
      Point saturn = new Point("Saturn");

      Point[] points = { sun, moon, mars, venera, saturn };
      sortArrayPoints(points);
      printPoints(points);
   }

   public static void printPoints(Point[] points) {
      for (int i = 0; i < points.length; i++) {
         System.out.printf("%s %d %d%n", points[i].getName(), points[i].getX(), points[i].getY());
      }
   }

   public static void sortArrayPoints(Point[] points) {
      for (int i = points.length - 1; i > 0; i--) {
         for (int j = 0; j < i; j++) {
            if (distance(points[j]) > distance(points[j + 1])) {
               Point temp = points[j];
               points[j] = points[j + 1];
               points[j + 1] = temp;
            }
         }
      }
   }

   public static double distance(Point point) {
      double distance = Math.sqrt(Math.pow(point.getX(), 2) + Math.pow(point.getY(), 2));
      return distance;
   }
}
