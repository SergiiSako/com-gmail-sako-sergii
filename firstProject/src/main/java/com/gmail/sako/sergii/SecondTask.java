package com.gmail.sako.sergii;

import java.util.Scanner;

public class SecondTask {
   public static void main(String[] args) {
      int c = 1;
      Scanner s = new Scanner(System.in);
      System.out.println("Введите значение A");
      double a = s.nextDouble();
      System.out.println("Введите значение B");
      double b = s.nextDouble();
      double sum = -1 / (a * a);
      System.out.println("1.15.a:  = " + sum);
      sum = a / (b * c);
      System.out.println("1.15.б:  = " + sum);
      sum = a / b * c;
      System.out.println("1.15.в:  = " + sum);
      sum = (a + b) / 2;
      System.out.println("1.15.г:  = " + sum);
      sum = 5.45 * ((a + (2 * b)) / (2 - a));
      System.out.println("1.15.д: = " + sum);
      double sum1 = b * b - 4 * a * c;
      double sqrt = Math.sqrt(sum1);
      sum = (-b + sum1) / 2 * a;
      System.out.println("1.15.е: = " + sum);
      sum = 1 / ((1 + 1 / (2 + 1 / (2 + 3 / 5))));
      System.out.println("1.15.ж: = " + sum);
      sum1 = Math.pow(a, b);
      sum = Math.pow(sum1, c);
      System.out.println("1.15.k: = " + sum);

   }

}
