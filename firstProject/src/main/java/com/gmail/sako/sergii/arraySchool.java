package com.gmail.sako.sergii;

public class arraySchool {

   public static void main(String[] args) {
      int[][] table = new int[11][4];
      fillTable(table);
      int[] sumsCol = sumCols(table);
      printResult(table, sumsCol);
      System.out.print("\n\nЧисленность самой большой (по количеству учащихся) параллели = " + max(sumsCol));
      System.out.print("\nЧисленность самой маленькой (по количеству учащихся) параллели = " + min(sumsCol));
   }

   static void printResult(int[][] table, int[] sumsCol) {
      for (int i = 0; i < table.length; i++) {
         for (int j = 0; j < table[i].length; j++) {
            System.out.print(table[i][j] + "  ");
         }
         System.out.print("\t\n");
      }
      System.out.println();
      for (int j = 0; j < sumsCol.length; j++) {
         System.out.print(sumsCol[j] + " ");
      }
   }

   static void fillTable(int[][] table) {
      for (int i = 0; i < table.length; i++) {
         for (int j = 0; j < table[i].length; j++) {
            table[i][j] = (int) (Math.random() * 10 + 20);
         }
      }
   }

   static int[] sumCols(int[][] table) {
      int[] sumsCol = new int[table[0].length];
      for (int j = 0; j < table[0].length; j++) {
         for (int i = 0; i < table.length; i++) {
            sumsCol[j] += table[i][j];
         }
      }
      return sumsCol;
   }

   static int min(int[] sumsCol) {
      return minMax(sumsCol, true);
   }

   static int max(int[] sumsCol) {
      return minMax(sumsCol, false);
   }

   static int minMax(int[] sumsCol, boolean isMin) {
      int result = sumsCol[0];
      for (int i = 1; i < sumsCol.length; i++) {
         if ((isMin && result > sumsCol[i]) || (!isMin && result < sumsCol[i])) {
            result = sumsCol[i];
         }
      }
      return result;
   }

}
