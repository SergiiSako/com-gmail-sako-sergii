package com.gmail.sako.sergii;

import java.util.*;

public class Formuls {
   public static void main(String[] args) {
      Scanner s = new Scanner(System.in);
      System.out.println("Введите значение X");
      double x = s.nextDouble();
      System.out.println("Введите значение Y");
      double y = s.nextDouble();
      double sum = 2 * x;
      System.out.println("1.14.a: = " + sum);
      double sin = Math.sin(Math.toRadians(x));
      System.out.println("1.14.б: = " + sin);
      sum = x * x;
      System.out.println("1.14.в: = " + sum);
      double sqrt = Math.sqrt(x);
      System.out.println("1.14.г: = " + sqrt);
      double abs = Math.abs(x);
      System.out.println("1.14.д: = " + abs);
      double cos = 5 * (Math.cos(Math.toRadians(y)));
      System.out.println("1.14.е: = " + cos);
      sum = -7.5 * (x * x);
      System.out.println("1.14.ж: = " + sum);
   }
}
