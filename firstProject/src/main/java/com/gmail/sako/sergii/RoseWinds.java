package com.gmail.sako.sergii;

public class RoseWinds {

   public static void main(String[] args) {
      int[] courseWinds = new int[365];
      int[] sumDayByYear = new int[8];
      // заполнение массива показаниями за год и подсчет по дням + заполнение массива
      // подсчитанными значениями
      for (int i = 0; i < courseWinds.length; i++) {
         courseWinds[i] = (int) (Math.random() * 8 + 1);
         sumDayByYear[courseWinds[i] - 1]++;
      }
      int indexMin = 0;
      int min = sumDayByYear[0];
      for (int i = 0; i < sumDayByYear.length; i++) {
         if (i == 0) {
            continue;
         }
         if (sumDayByYear[i] < min) {
            indexMin = i;
            min = sumDayByYear[i];
         }
      }

      if (indexMin == 0) {
         System.out.print(
               "Рекомендованное месторасположение комплекса, т.к. частота ветра была минимальной со стороныи 1 - северный");

      } else if (indexMin == 1) {
         System.out.print(
               "Рекомендованное месторасположение комплекса, т.к. частота ветра была минимальной со стороныи 2 - южный");

      } else if (indexMin == 2) {
         System.out.print(
               "Рекомендованное месторасположение комплекса, т.к. частота ветра была минимальной со стороныи 3 - восточный");

      } else if (indexMin == 3) {
         System.out.print(
               "Рекомендованное месторасположение комплекса, т.к. частота ветра была минимальной со стороныи 4 - западный");

      } else if (indexMin == 4) {
         System.out.print(
               "Рекомендованное месторасположение комплекса, т.к. частота ветра была минимальной со стороныи 5 - северо-западный");

      } else if (indexMin == 5) {
         System.out.print(
               "Рекомендованное месторасположение комплекса, т.к. частота ветра была минимальной со стороныи 6 - северо-восточный");

      } else if (indexMin == 6) {
         System.out.print(
               "Рекомендованное месторасположение комплекса, т.к. частота ветра была минимальной со стороныи 7 - юго-западный");

      } else if (indexMin == 7) {
         System.out.print(
               "Рекомендованное месторасположение комплекса, т.к. частота ветра была минимальной со стороныи 8 - юго-восточный");
      }
   }
}
