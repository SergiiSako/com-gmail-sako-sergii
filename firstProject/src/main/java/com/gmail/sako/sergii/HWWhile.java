package com.gmail.sako.sergii;

public class HWWhile {

   public static void main(String[] args) {
      double trip = 10;
      int day = 0;
      double sumTrip = trip;
      
      while (day < 10) {
         day++;
         trip += trip * 0.1;
           if (day <= 7)
              sumTrip += trip;
           if (day == 1)
              continue;
           System.out.printf("День тренировки %d, Пробег лыжника = %.2f", day, trip);
           System.out.println();
      }   
           System.out.printf("Пробег лыжника = %.2f км", sumTrip );
   }
}