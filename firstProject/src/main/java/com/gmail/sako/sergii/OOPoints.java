package com.gmail.sako.sergii;

public class OOPoints {

   public static void main(String[] args) {
      
      Point sun = new Point("Sun", 10, 10);
      Point moon = new Point("Moon", 1, 2);
      Point mars = new Point("Mars", 2, 1);
      Point venera = new Point("Venera", 3, 4);
      Point saturn = new Point("Saturn");

      Point[] points = { sun, moon, mars, venera, saturn };
      sortArrayPoints(points);
      printPoints(points);
   }

   static void printPoints(Point[] points) {
      for (int i = 0; i < points.length; i++) {
         System.out.printf("%s %d %d%n", points[i].name, points[i].x, points[i].y);
      }
   }

   static void sortArrayPoints(Point[] points) {
      for (int i = points.length - 1; i > 0; i--) {
         for (int j = 0; j < i; j++) {
            if (distance(points[j]) > distance(points[j + 1])) {
               Point temp = points[j];
               points[j] = points[j + 1];
               points[j + 1] = temp;
            }
         }
      }
   }

   static double distance(Point point) {
      double distance = Math.sqrt(Math.pow(point.x, 2) + Math.pow(point.y, 2));
      return distance;
   }
}

class Point {
   String name;
   int x = 0;
   int y = 0;

   Point(String name) {
      this(name, 0, 0);
   }

   Point(String name, int x, int y) {
      this.name = name;
      this.x = x;
      this.y = y;
   }
}

