package com.gmail.sako.sergii;

import java.util.Arrays;

public class Array {
   public static void main(String[] args) {
      int[] years = new int [30];
      // заполнение
      for (int i = 0; i < years.length; i++) {
         years[i] = (int) (Math.random() * 60 + 1950 );
      }
      // вывод заполненного массива
      //System.out.println(Arrays.toString(years));
      boolean isSorted = false;
      int buf;
      while(!isSorted) {
         isSorted = true;
         for (int i = 0; i < years.length-1; i++) {
            if(years[i] > years[i+1]){
               isSorted = false;
               buf = years[i];
               years[i] = years[i+1];
               years[i+1] = buf;
            }
         }
      }
      //вывод отсортированного массива
      //System.out.println(Arrays.toString(years));
      //вывод старших по возрасту людей
      System.out.println(years [0]);
      System.out.println(years [1]);
   }
}
