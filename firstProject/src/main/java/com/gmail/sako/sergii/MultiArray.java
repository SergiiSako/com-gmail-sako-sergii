package com.gmail.sako.sergii;

public class MultiArray {

   public static void main(String[] args) {
      int sum = 0;
      int size = 9;
      int[] multiarray[] = new int[size][size];
      // заполнение массива
      for (int i = 0; i < size; i++) {
         for (int j = 0; j < size; j++) {
            multiarray[i][j] = (int) (Math.random() * 10);
         }
      }
      // подсчет суммы всех элементов массива
      for (int i = 0; i < size; i++) {
         for (int j = 0; j < size; j++) {
            sum += multiarray[i][j];
         }
      }
      // вывод массива
      for (int i = 0; i < size; i++) {
         for (int j = 0; j < size; j++) {
            System.out.print(multiarray[i][j] + "\t");
         }
         System.out.println();
      }
      System.out.println("Сумма всех элементов массива = " + sum);
   }
}