package com.gmail.sako.sergii;

import java.util.Arrays;

public class ArrayPlus {

   public static void main(String[] args) {
      // инициализация массива
      int[] array = new int[10];
      // заполнение с помощью метода и вывод
      System.out.println(Arrays.toString(makeArr(array)));
      // поис пары с нечетними эл-ми
      int index = 0;
      for (int i = 0; i < array.length - 1; i++) {
         if (array[i] % 2 != 0 && array[i + 1] % 2 != 0) {
            index = i;
            System.out.print("Пара найдена.\nИндекс первого элемента = " + index + "\nИндекс второго = " + ++index);
            break;
         }
      }
      if (index == 0) {
         System.out.println("Пара не найдена");
      }
   }

   static int[] makeArr(int[] array) {
      for (int i = 0; i < array.length; i++) {
         array[i] = (int) (Math.random() * 10);
      }
      return array;
   }
}
