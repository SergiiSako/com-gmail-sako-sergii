package com.gmail.sako.sergii;

import java.math.*;

public class Chess {

   public static void main(String[] args) {
      int a = 2; 
      int b = 2;
      int c = 1; 
      int d = 3;
      boolean numbers = 
         1 <= a && a <= 8 && 
         1 <= b && b <= 8 &&
         1 <= c && c <= 8 &&
         1 <= d && d <= 8;
//    3.34а
      boolean ladia = numbers && a == c || b == d;
         System.out.println("3.34а - существует угроза удара ладьей: " + ladia);
      
//    3.34б 
      boolean slon = numbers && 
         (Math.abs(a - c) == Math.abs(b - d));
         System.out.println("3.34б - существует угроза удара слоном: " + slon);
      
//    3.34в
      boolean king = numbers && (Math.abs(a-c) <= 1) && (Math.abs(b-d) <= 1);
         System.out.println("3.34в - существует угроза удара королем: " + king);
//    3.34г
      boolean queen = ladia || slon || king;
         System.out.println("3.34г - существует угроза удара ферзем: " + queen);
//    3.34д
      boolean whitePeshka = numbers && (Math.abs(a-c) <= 1 && (b + 1 == d));
         System.out.println("3.34д - существует угроза удара белой пешкой: " +  whitePeshka);
//    3.34е
      boolean blackPeshka = numbers && (Math.abs(a-c) <= 1 && (d - 1) == b);
         System.out.println("3.34е - существует угроза удара черной пешкой: " +  blackPeshka);        
         
   }
}
