package com.gmail.sako.sergii;

public class BinarySearch {

   public static void main(String[] args) {

      int[] data = { 2, 5, 7, 8, 9, 15, 31, 44 };
      final int ELEMENT = 44;
      System.out.println(binarySearch(data, ELEMENT));
   }

   public static boolean binarySearch(int[] data, int element) {
      return binarySearch(data, element, 0, data.length - 1);
   }

   private static boolean binarySearch(int[] data, int element, int from, int to) {

      if (from == to) {
         return data[0] == element;
      }

      int middle = ((from + to) + 1) / 2;

      if (data[middle] == element) {
         return true;

      } else if (data[middle] > element) {
         return binarySearch(data, element, from, middle - 1);
      } else {
         return binarySearch(data, element, middle, to);
      }
   }
}