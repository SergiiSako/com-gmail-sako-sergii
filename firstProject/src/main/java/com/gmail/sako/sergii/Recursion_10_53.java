package com.gmail.sako.sergii;

public class Recursion_10_53 {

   public static void main(String[] args) {
      int num = Integer.parseInt(args[0]);
      System.out.println(num);
      recursion(num);
   }

   static void recursion(int n) {
      if (n / 10 == 0 && n % 10 == 0) {
         return;
      } else {
         System.out.print(n % 10);
         recursion(n / 10);
      }
   }
}
