package com.gmail.sako.sergii;

import java.util.Arrays;

public class nextArray {
	public static void main(String[] args) {
		int[] array = new int[10];
		int counter = 0;
		final int element = 11;
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) (Math.random() * 10 - 5);
			if (i >= 1) {
				if ((array[i - 1] < 0 && array[i] < 0) || (array[i - 1] > 0 && array[i] > 0)) {
					counter++;
				}
			}
		}
		System.out.println("Заданый массив: " + Arrays.toString(array));
		int[] arr = new int[array.length + counter];
		System.out.println("Количество пар с одинаковым знаком: " + counter);
		for (int i = 0, j = 0; j < arr.length; i++, j++) {
			if (i >= 1) {
				if ((array[i - 1] < 0 && array[i] < 0) || (array[i - 1] > 0 && array[i] > 0)) {
					arr[j] = element;
					j++;
				}
			}
			arr[j] = array[i];
		}
		System.out.println("Массив с вставками: " + Arrays.toString(arr));
	}
}