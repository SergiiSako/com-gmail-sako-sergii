package ua.ort.javabase.entities;

import org.junit.Assert;
import org.junit.Test;

public class PolygonImplTest {

   @Test
   public void distanceTest() {
      Point a = new Point("a", 1, 1);
      Point b = new Point("b", 2, 1);
      double distance = 0;
      distance = Math.sqrt(Math.pow((b.getX() - a.getX()), 2) + Math.pow((b.getY() - a.getY()), 2));
      Assert.assertEquals(1, distance, 0);
   }
}
