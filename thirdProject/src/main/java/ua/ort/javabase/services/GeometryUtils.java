package ua.ort.javabase.services;

import ua.ort.javabase.entities.Point;
import ua.ort.javabase.entities.Polygon;

public class GeometryUtils {
   
   public static double distance(Point a, Point b) {
      return Math.sqrt(Math.pow((b.getX() - a.getX()), 2) + Math.pow((b.getY() - a.getY()), 2));

   }
}
   
