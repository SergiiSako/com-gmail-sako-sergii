package ua.ort.javabase.services.checkers;

import ua.ort.javabase.entities.Point;
import ua.ort.javabase.entities.Polygon;
import ua.ort.javabase.entities.Square;
import ua.ort.javabase.services.GeometryUtils;

public class SquareChecker implements PolygonChecker {

   public boolean check(Point[] points) {

      if (points.length == 4) {
         double l1 = 0;
         double l2 = 0;
         double l3 = 0;
         double l4 = 0;

//    нахождение расстояния между точками LowLeft - UpperLeft 
         l1 = GeometryUtils.distance(points[0], points[1]);
//    нахождение расстояния между точками UpperLeft - UpperRight
         l2 = GeometryUtils.distance(points[1], points[2]);
//    нахождение расстояния между точками UpperRight - LowRight
         l3 = GeometryUtils.distance(points[3], points[2]);
//    нахождение расстояния между точками LowRight - LowLeft
         l4 = GeometryUtils.distance(points[0], points[3]);

         if (l1 == l2 && l2 == l3 && l3 == l4) {
            return true;
         }

         return false;
      }
      return false;
   }

   public Polygon create(Point[] points) {

      return new Square(points[0], points[1], points[2], points[3]);
   }

}
