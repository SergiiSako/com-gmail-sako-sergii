package ua.ort.javabase.services;

import ua.ort.javabase.entities.Point;
import ua.ort.javabase.entities.Polygon;
import ua.ort.javabase.services.checkers.CheckerManager;
import ua.ort.javabase.services.checkers.PolygonChecker;

public class PolygonBuilder {
   public Polygon createPolygon(Point[] points) {
      PolygonChecker[] checkers = CheckerManager.getCheckers();
      for (int i = 0; i < checkers.length; i++) {
         if (checkers[i].check(points)) {
            return checkers[i].create(points);
         }
      }
      return null;
   }
}
