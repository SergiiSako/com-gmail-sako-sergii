package ua.ort.javabase.services.checkers;

import ua.ort.javabase.entities.Point;
import ua.ort.javabase.entities.Polygon;

public interface PolygonChecker {
   public boolean check(Point[] points);
   public Polygon create(Point[] points);
}
