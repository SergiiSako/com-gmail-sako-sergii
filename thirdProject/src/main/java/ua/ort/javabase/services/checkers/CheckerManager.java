package ua.ort.javabase.services.checkers;

public class CheckerManager {
   private static PolygonChecker[] checkers;

   static {
      checkers = new PolygonChecker[2];
      checkers[0] = new SquareChecker();
      checkers[1] = new RectangleChecker();
   }

   public static PolygonChecker[] getCheckers() {
      PolygonChecker[] copy = new PolygonChecker[checkers.length];
      System.arraycopy(checkers, 0, copy, 0, checkers.length);
      return copy;
   }
}
