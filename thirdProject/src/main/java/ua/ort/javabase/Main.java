package ua.ort.javabase;

import ua.ort.javabase.entities.Circle;
import ua.ort.javabase.entities.Point;
import ua.ort.javabase.entities.Polygon;
import ua.ort.javabase.services.PolygonBuilder;

public class Main {

   public static void main(String[] args) {

      Point ll = new Point("1", 3, 3);
      Point ul = new Point("2", 3, 6);
      Point ur = new Point("3", 6, 6);
      Point lr = new Point("4", 6, 3);

      Point q = new Point("5", 9, 6);
      Point qq = new Point("6", 9, 3);

      PolygonBuilder pb = new PolygonBuilder();

      Polygon p1 = pb.createPolygon(new Point[] { ll, ul, ur, lr });
      System.out.println(p1.getArea());

      Polygon p2 = pb.createPolygon(new Point[] { ll, ul, q, qq });
      System.out.println(p2.getArea());

      Circle cir = new Circle(ul, 3);
      System.out.println(cir.getArea());

   }

}
