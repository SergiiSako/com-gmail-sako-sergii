package ua.ort.javabase.entities;

public abstract class Figure {

   abstract double getArea();

}