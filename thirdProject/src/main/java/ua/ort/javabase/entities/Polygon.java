package ua.ort.javabase.entities;

public abstract class Polygon extends Figure implements Cloneable {

   public abstract double getArea();

   public abstract int getNumberOfCorners();

   @Override
   protected Polygon clone() throws CloneNotSupportedException {
      return (Polygon) super.clone();
   }

}