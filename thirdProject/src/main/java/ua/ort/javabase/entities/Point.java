package ua.ort.javabase.entities;

public class Point implements Cloneable {

   private String name;
   private int x = 0;
   private int y = 0;

   public int getX() {
      return x;
   }

   public int getY() {
      return y;
   }

   public String getName() {
      return name;
   }

   public Point(String name, int x, int y) {
      this.name = name;
      this.x = x;
      this.y = y;
   }

   @Override
   public String toString() {
      return "Point [name=" + name + ", x=" + x + ", y=" + y + "]";
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((name == null) ? 0 : name.hashCode());
      result = prime * result + x;
      result = prime * result + y;
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      Point other = (Point) obj;
      if (name == null) {
         if (other.name != null)
            return false;
      } else if (!name.equals(other.name))
         return false;
      if (x != other.x)
         return false;
      if (y != other.y)
         return false;
      return true;
   }

   @Override
   protected Point clone() throws CloneNotSupportedException {
      return (Point) super.clone();
   }

}
