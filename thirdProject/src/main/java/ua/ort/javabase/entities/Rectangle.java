package ua.ort.javabase.entities;

import ua.ort.javabase.services.GeometryUtils;

public class Rectangle extends Polygon implements Cloneable{

   private final Point ul;
   private final Point ur;
   private final Point ll;
   private final Point lr;

   protected double area;

   public Rectangle(Point ll, Point ul, Point ur, Point lr) {
      super();
      this.ul = ul;
      this.ur = ur;
      this.ll = ll;
      this.lr = lr;
   }

   @Override
   public double getArea() {
      return area = (GeometryUtils.distance(ll, ul) * GeometryUtils.distance(ul, ur));
   }

   @Override
   public int getNumberOfCorners() {
      return 4;
   }

   public Point getUl() {
      return ul;
   }

   public Point getUr() {
      return ur;
   }

   public Point getLl() {
      return ll;
   }

   public Point getLr() {
      return lr;
   }

   @Override
   protected Rectangle clone() throws CloneNotSupportedException {
      return (Rectangle) super.clone();
   }

}