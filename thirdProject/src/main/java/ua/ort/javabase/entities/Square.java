package ua.ort.javabase.entities;

import ua.ort.javabase.services.GeometryUtils;

public class Square extends Rectangle implements Cloneable{

   public Square(Point ll, Point ul, Point ur, Point lr) {
      super(ll, ul, ur, lr);
   }

   @Override
   public double getArea() {
      return area = (Math.pow(GeometryUtils.distance(super.getLl(), super.getUl()), 2));
   }

   @Override
   public String toString() {
      return "Square [toString()=" + super.toString() + "]";
   }

   @Override
   protected Square clone() throws CloneNotSupportedException {
      return (Square) super.clone();
   }

}
