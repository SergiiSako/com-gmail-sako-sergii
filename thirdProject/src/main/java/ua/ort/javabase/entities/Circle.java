package ua.ort.javabase.entities;

public class Circle extends Figure implements Cloneable {
   private Point a;
   double radius;

   public Circle(Point a, double radius) {
      this.a = a;
      this.radius = radius;
   }

   @Override
   public double getArea() {
      return Math.PI * Math.pow(radius, 2);
   }

   @Override
   protected Circle clone() throws CloneNotSupportedException {
      return (Circle) super.clone();
   }

}
